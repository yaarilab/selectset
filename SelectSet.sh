#!/bin/bash

## handles cmd options
while getopts ":f:r:a:ciso:" option ; do
        case "${option}" in
                f) R1_FILE=${OPTARG};;    # R1 file 
                r) R2_FILE=${OPTARG};;    # R2 file 
                a) ALLELE_FILE=${OPTARG};;     # fasta file 
        esac
done


usage() { echo "Usage: $0 [-f R1.fastq] [-r R2.fastq] [-a allele_file.fasta]" 1>&2; exit 1; }
if [ -z "$R1_FILE" ] || [ -z "$R2_FILE" ]  || [ -z "$ALLELE_FILE" ]; then
    usage
fi
echo "R1=$R1_FILE,R2=$R2_FILE,ALLELES=$ALLELE_FILE" 




##########################################################################################
#############################          local Parameters           ##############################
##########################################################################################

RUN="nice -19"
export LD_LIBRARY_PATH=/private/anaconda3/lib
export IGDATA=/home/bcrlab/eitan/data/igblast/
IGBLAST_PATH=/private/tools/igblast/ncbi-igblast-1.16.0/bin/            # change to your local igblast path
BLAST_PATH=/private/tools/ncbi-blast/bin/
MAXCORES=24

QUAL=20

####################################################################
### system parameters  - DO NOT CHANGE THIS     ####################
NPROC=3  # DO NOT CHANGE THIS ! may crash cluster if increased 
if [ "$MAXCORES" -gt 40 ] ; then  ## DO NOT CHANGE THIS ! may crash cluster if increased
    echo "MAXCORES exceeded maximum allowed"; exit 1;
fi
    


##########################################################################################
################################          START           ################################
##########################################################################################

#NEW ALLELE SCRIPT:
    name1=$(basename $R1_FILE | sed 's/.fastq//')
    name2=$(basename $R2_FILE | sed 's/.fastq//')
#CREATE NEW DIRECTORY
    folder=$(basename $R1_FILE | sed 's/.fastq//' | sed 's/_R[0-9]//')'/'
    mkdir $folder
    echo "Which process would you like to use for mergging? (1 - for ASSEMBLEPAIRS, 2 - for PANDASEQ")
	read mergging_process
	case $mergging_process in
	
	1)	#MERGING WITH ASSEMBLEPAIRS:
		echo "merge files with assemblepairs"
		$RUN FilterSeq.py quality -s $R1_FILE $R2_FILE -q $QUAL
		$RUN FilterSeq.py quality -s $R2_FILE -q $QUAL --nproc $MAXCORES
		$RUN PairSeq.py -1 $name1"_quality-pass.fastq" -2 $name2"_quality-pass.fastq" --coord illumina
		AssemblePairs.py align -1 $name1'_quality-pass.fastq' -2 $name2'_quality-pass.fastq' -o $folder$name1'_merged_seq.fasta' --fasta --coord illumina 
		;;
	2)	#MERGING WITH PANDASEQ:
		echo "merge files with pandaseq"
		#$RUN pandaseq -f $name1'_quality-pass.fastq' -r $name2'_quality-pass.fastq' -d rbfkms -w  $name1'_merged_seq.fasta' -T $MAXCORES -g 'pandaseq_log_'$name1'.log'
		$RUN pandaseq -f $R1_FILE -r $R2_FILE -d rbfkms -w  $folder$name1'_merged_seq.fasta' -T $MAXCORES -g $folder'pandaseq_log_'$name1'.txt'
		;;
	esac
#BLASTN PROCESS:
    echo "Run blastn on merged sequences"
    $RUN $BLAST_PATH'makeblastdb' -in $ALLELE_FILE -dbtype 'nucl' -out $folder$name1'_blastn_new_allele_db' > $folder'makedb_'$name1'.log' 2>&1
    #$RUN $BLAST_PATH'blastn' -query $name1'_merged_seq.fasta' -db $folder$name1'_blastn_new_allele_db' -outfmt 7 -out $folder#$name1'_blastn_output.out' -num_threads $MAXCORES
    $RUN $BLAST_PATH'blastn' -query $name1'_merged_seq.fasta' -db $folder$name1'_blastn_new_allele_db' -outfmt '7 std qseq sseq btop' -out $folder$name1'_blastn_output.out' -num_threads $MAXCORES
    echo "blastn run finished"
    ALLELES=($(grep -e ">" $ALLELE_FILE --no-group-separator | awk 'sub(/^>/, "")'))
    SEQS=($(grep -v "^\>" $ALLELE_FILE))
    for ((i = 0; i < ${#ALLELES[@]}; ++i)); do
        allele=${ALLELES[$i]}
        seq=${SEQS[$i]}
        echo "Creating R1/2 files for allele "$allele
        seq_id_file='seq_id_'$name1'_'$allele
        $RUN grep -F "$allele" $folder$name1'_blastn_output.out'| awk '$3>99.5'  > $folder$seq_id_file'.tab'
        #printf '%s\n' "${seq_ids[@]}" > $folder$seq_id_file'.txt'
        $RUN python filter_selectset_blastn.py -f $folder$seq_id_file".tab" -r $seq -a $allele -s $folder$seq_id_file'.txt'
        grep -A 3 -F -f $folder$seq_id_file'_regex.txt' $R1_FILE --no-group-separator >  $folder$name1"_"$allele".fastq"
        grep -A 3 -F -f $folder$seq_id_file'_regex.txt' $R2_FILE --no-group-separator >  $folder$name2"_"$allele".fastq"
     done

