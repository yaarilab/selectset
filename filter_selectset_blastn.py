#!/usr/bin/python
import optparse
import sys
import os 
import pandas
import re
import operator
from Bio.Seq import Seq

# Get the arguments list 
cmdargs = sys.argv

parser = optparse.OptionParser()

parser.add_option('-s', '--sample_name',
    action="store",
    help="sample name", default="seq_ids")

parser.add_option('-f', '--blast_file',
    action="store",
    help="blastn out file", default="")

parser.add_option('-r', '--ref_seq',
    action="store",
    help="reference sequence", default="")

parser.add_option('-a', '--novel_allele_name',
    action="store",
    help="novel allele name", default="")

cmdargs, options = parser.parse_args()



if not cmdargs.blast_file:
    parser.error("no blasn file was supplied")

if not cmdargs.ref_seq:
    parser.error("no reference sequence was supplied")

if not cmdargs.novel_allele_name:
    parser.error("no novel allele name was supplied")

#print(cmdargs)

### filter blastn file
## read file

blast_out=pandas.read_csv(cmdargs.blast_file,sep="\t",header=None)

## read reference allele and novel allele snps

ref = cmdargs.ref_seq.replace('.', '')
ref_reverse = str(Seq(ref).reverse_complement())
snps = cmdargs.novel_allele_name.split('*')[1].split("_")[1:]
indxs = [int(re.sub('[A-Z]','',i)) for i in snps]
indxs_ref = [i-cmdargs.ref_seq[0:i].count('.') for i in indxs]
nuc_ref = [cmdargs.ref_seq[i] for i in indxs]
indxs_ref_reverse = [len(ref)-i for i in indxs_ref]
nuc_ref_reverse = [ref_reverse[i] for i in indxs_ref_reverse]
## iterate over the rows, for each row check if novel allele exists
ids = []
for index, row in blast_out.iterrows():		
	if row[3] < 100:
		continue
	## check the orientation of the sequence and get bounds
	direction = row[8] < row[9]
	low = row[8] if direction else row[9]
	up  = row[9] if direction else row[8]
	## cut reference seq to range
	ref_seq = ref[low:up]
	ref_seq = ref_seq if direction else str(Seq(ref_seq).reverse_complement())

	## add dots where nuc are missing
	q_seq = row[12]
	add_up = '.'*(len(ref)-up)
	add_low = '.'*(low-1)
	q_seq = add_up+q_seq+add_low
	## grep reference seq in query seq
	## comapre by direction of sequence
	if direction:
		## normal
		## check if novel allele index are in range of query sequence
		if all([x in range(low, up) for x in indxs_ref]):
			## get nuc of query sequence
			q_seq_nuc = operator.itemgetter(*indxs_ref)(q_seq)
			## comapre snps
			if all([True if i == j else False for i, j in zip(q_seq_nuc, nuc_ref)]):
				ids.append(row[0])
				if row[0]=="M00619:242:000000000-AP65B:1:2109:22113:19409:26;0.817745":
					print("Fail")
		else:
			continue
	else:
		## reverse
		## check if novel allele index are in range of query sequence
		if all([x in range(low, up) for x in indxs_ref_reverse]):
			## get nuc of query sequence
			q_seq_nuc = operator.itemgetter(*indxs_ref_reverse)(q_seq)
			## comapre snps
			if all([True if i == j else False for i, j in zip(q_seq_nuc, nuc_ref_reverse)]):
				ids.append(row[0])
				if row[0]=="M00619:242:000000000-AP65B:1:2109:22113:19409:26;0.817745":
					print("Fail")
		else:
			continue

		
## get sequence ids that match reference seq
seq_ids = [seq_id.split(";")[0].rsplit(":",1)[0] for seq_id in ids]

## save sequence id to file
with open(os.path.splitext(cmdargs.sample_name)[0]+"_regex.txt", mode='wt', encoding='utf-8') as outfile:
    outfile.write('\n'.join(seq_ids))
