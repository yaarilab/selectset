#!/bin/bash

## handles cmd options
while getopts ":f:r:a:m:ciso:" option ; do
        case "${option}" in
                f) R1_FILE=${OPTARG};;    # R1 file 
                r) R2_FILE=${OPTARG};;    # R2 file 
                a) ALLELE_FILE=${OPTARG};;     # fasta file 
                m) MERGE=${OPTARG};;     # mergging process 
        esac
done


usage() { echo "Usage: $0 [-f R1.fastq] [-r R2.fastq] [-a allele_sequence_file.fasta] [-m fastq merger: 1 - for PANDASEQ, 2 - for ASSEMBLEPAIRS]" 1>&2; exit 1; }

if [ -z "$MERGE" ]; then
    MERGE=1
fi

if [ -z "$R1_FILE" ] || [ -z "$R2_FILE" ]  || [ -z "$ALLELE_FILE" ]; then
    usage
fi

merger_options=("" "PANDASEQ" "ASSEMBLEPAIRS")
echo "R1 file=$R1_FILE, R2 file=$R2_FILE, Allele file=$ALLELE_FILE, Fastq Merger=${merger_options[$MERGE]}"




##########################################################################################
#############################          local Parameters           ##############################
##########################################################################################

RUN="nice -19"
BLAST_PATH=/usr/local/bin/
MAXCORES=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || sysctl -n hw.ncpu)

QUAL=20

####################################################################
### system parameters  - DO NOT CHANGE THIS     ####################
if [ "$MAXCORES" -gt 40 ] ; then  ## DO NOT CHANGE THIS ! may crash cluster if increased
    echo "MAXCORES exceeded maximum allowed"; exit 1;
fi
    


##########################################################################################
################################          START           ################################
##########################################################################################

#NEW ALLELE SCRIPT:
    name1=$(basename $R1_FILE | sed 's/.fastq//')
    name2=$(basename $R2_FILE | sed 's/.fastq//')
#CREATE NEW DIRECTORY
    dir=$(dirname $R1_FILE)
    folder=$dir/tmp/
    mkdir -p $folder
    mkdir -p $dir/submission_set/
	case "$MERGE" in
	
	1)	#MERGING WITH PANDASEQ:
		echo "merge files with pandaseq"
		$RUN pandaseq -f $R1_FILE -r $R2_FILE -d rbfkms -w  $folder$name1'_merged_seq.fasta' -T $MAXCORES -g $folder'pandaseq_log_'$name1'.txt'
		;;
	2)	#MERGING WITH ASSEMBLEPAIRS:
		echo "merge files with assemblepairs"
		$RUN FilterSeq.py quality -s $R1_FILE $R2_FILE -q $QUAL --nproc $MAXCORES
		$RUN PairSeq.py -1 $name1"_quality-pass.fastq" -2 $name2"_quality-pass.fastq" --coord illumina
		$RUN AssemblePairs.py align -1 $name1'_quality-pass.fastq' -2 $name2'_quality-pass.fastq' -o $folder$name1'_merged_seq.fasta' --fasta --coord illumina 
		;;
	esac
#BLASTN PROCESS:
    echo "Run blastn on merged sequences"
    $RUN makeblastdb -in $ALLELE_FILE -parse_seqids -dbtype 'nucl' -out $folder$name1'_blastn_new_allele_db' > $folder'makedb_'$name1'.log' 2>&1
    $RUN blastn -query $folder$name1'_merged_seq.fasta' -db $folder$name1'_blastn_new_allele_db' -outfmt '7 std qseq sseq btop' -out $folder$name1'_blastn_output.out' -num_threads $MAXCORES
    echo "blastn run finished"
    ALLELES=($(grep -e ">" $ALLELE_FILE --no-group-separator | awk 'sub(/^>/, "")'))
    SEQS=($(grep -v "^>" $ALLELE_FILE))
    for ((i = 0; i < ${#ALLELES[@]}; ++i)); do
        allele=${ALLELES[$i]}
        seq=${SEQS[$i]}
        echo "Creating R1/2 files for allele "$allele
        seq_id_file=seq_id_$name1_$allele
        $RUN grep -F "$allele" $folder$name1'_blastn_output.out'| awk '$3>99.5'  > $folder$seq_id_file'.tab'
        $RUN python /usr/local/bin/filter_selectset_blastn.py -f $folder$seq_id_file".tab" -r $seq -a $allele -s $folder$seq_id_file'.txt'
        allele=$(basename $allele | sed 's/[*]/_/')
        grep -A 3 -F -f $folder$seq_id_file'_regex.txt' $R1_FILE --no-group-separator >  $dir/submission_set/$name1'_'$allele.fastq
        grep -A 3 -F -f $folder$seq_id_file'_regex.txt' $R2_FILE --no-group-separator >  $dir/submission_set/$name2'_'$allele.fastq
        gzip -f $dir/submission_set/$name1'_'$allele.fastq
        gzip -f $dir/submission_set/$name2'_'$allele.fastq
        md5sum $dir/submission_set/$name1'_'$allele.fastq.gz > $dir/submission_set/$name1'_'$allele'_'md5sum.txt
        md5sum $dir/submission_set/$name2'_'$allele.fastq.gz > $dir/submission_set/$name2'_'$allele'_'md5sum.txt
     done
    rm -r $folder

