#!/bin/bash

## handles cmd options
while getopts ":f:r:a:ciso:" option ; do
        case "${option}" in
                f) R1_FILE=${OPTARG};;    # R1 file 
                r) R2_FILE=${OPTARG};;    # R2 file 
                a) ALLELE_FILE=${OPTARG};;     # fasta file 
        esac
done


usage() { echo "Usage: $0 [-f R1.fastq] [-r R2.fastq] [-a allele_sequence_file.fasta]" 1>&2; exit 1; }
if [ -z "$R1_FILE" ] || [ -z "$R2_FILE" ]  || [ -z "$ALLELE_FILE" ]; then
    usage
fi
echo "R1=$R1_FILE,R2=$R2_FILE,ALLELES=$ALLELE_FILE" 




##########################################################################################
#############################          local Parameters           ##############################
##########################################################################################

RUN="nice -19"
export LD_LIBRARY_PATH=/private/anaconda3/lib
export IGDATA=/home/bcrlab/eitan/data/igblast/
IGBLAST_PATH=/private/tools/igblast/ncbi-igblast-1.16.0/bin/            # change to your local igblast path
BLAST_PATH=/private/tools/ncbi-blast/bin/
MAXCORES=24

QUAL=20

####################################################################
### system parameters  - DO NOT CHANGE THIS     ####################
NPROC=3  # DO NOT CHANGE THIS ! may crash cluster if increased 
if [ "$MAXCORES" -gt 40 ] ; then  ## DO NOT CHANGE THIS ! may crash cluster if increased
    echo "MAXCORES exceeded maximum allowed"; exit 1;
fi
    


##########################################################################################
################################          START           ################################
##########################################################################################

#NEW ALLELE SCRIPT:
    name1=$(basename $R1_FILE | sed 's/.fastq//')
    name2=$(basename $R2_FILE | sed 's/.fastq//')
#CREATE NEW DIRECTORY
    folder=$(basename $R1_FILE | sed 's/.fastq//' | sed 's/_R[0-9]//')'/'
    mkdir -p $folder
    mkdir -p submission
#MERGING WITH PANDASEQ:
    echo "merge files with pandaseq"
    if [ ! -f $folder$name1'_merged_seq.fasta' ]; then
        $RUN pandaseq -f $R1_FILE -r $R2_FILE -d rbfkms -w  $folder$name1'_merged_seq.fasta' -T $MAXCORES -g $folder'pandaseq_log_'$name1'.log'
    fi
#BLASTN PROCESS:
    echo "Run blastn on merged sequences"
    if [ ! -f $folder$name1'_blastn_output.out' ]; then
        $RUN $BLAST_PATH'makeblastdb' -in $ALLELE_FILE -dbtype 'nucl' -out $folder$name1'_blastn_new_allele_db' > $folder'makedb_'$name1'.log' 2>&1
        $RUN $BLAST_PATH'blastn' -query $folder$name1'_merged_seq.fasta' -db $folder$name1'_blastn_new_allele_db' -outfmt '7 std qseq sseq btop' -out $folder$name1'_blastn_output.out' -num_threads $MAXCORES
    fi
    echo "blastn run finished"
    ALLELES=($(grep -e ">" $ALLELE_FILE --no-group-separator | awk 'sub(/^>/, "")'))
    SEQS=($(grep -v "^>" $ALLELE_FILE))
    for ((i = 0; i < ${#ALLELES[@]}; ++i)); do
        allele=${ALLELES[$i]}
        seq=${SEQS[$i]}
        echo "Creating R1/2 files for allele "$allele
        seq_id_file='seq_id_'$name1'_'$allele
        $RUN grep -F "$allele" $folder$name1'_blastn_output.out'| awk '$3>99.5'  > $folder$seq_id_file'.tab'
        $RUN python filter_selectset_blastn.py -f $folder$seq_id_file".tab" -r $seq -a $allele -s $folder$seq_id_file'.txt'
        allele=$(basename $allele | sed 's/[*]/_/')
        grep -A 3 -F -f $folder$seq_id_file'_regex.txt' $R1_FILE --no-group-separator >  $folder$name1"_"$allele".fastq"
        grep -A 3 -F -f $folder$seq_id_file'_regex.txt' $R2_FILE --no-group-separator >  $folder$name2"_"$allele".fastq"
        rsync --info=progress2 -auzq $folder$name1"_"$allele".fastq" submission
        rsync --info=progress2 -auzq $folder$name2"_"$allele".fastq" submission
        gzip -f submission/$name1"_"$allele".fastq"
        gzip -f submission/$name2"_"$allele".fastq"
        md5sum submission/$name1"_"$allele".fastq.gz" > submission/$name1"_"$allele".txt"
        md5sum submission/$name2"_"$allele".fastq.gz" > submission/$name2"_"$allele".txt"
     done

