# selectSet #

The script in the repository creates a select set of novel allele sequences from 'raw data'.

It runs pandaseq package, more information on installing pandaseq can be found here: https://github.com/neufeld/pandaseq/wiki/Installation

## Input and output for SelectSet.sh:
### Input arguments
* -f forward (R1) fastq sequence file. Example P1_I1_S1_R1.fastq
* -r reverse (R2) fastq sequence file. Example P1_I1_S1_R2.fastq
* -a Novel allele reference fasta file. Example for such file is in the repository (novel_alleles.fasta)

The output is a folder containing the forward and reverse raw fastq files for each novel allele with the selected sequences.
### output files
* P1_I1_S1_R1_IGHV5-51*01_C45G.fastq
* P1_I1_S1_R2_IGHV5-51*01_C45G.fastq

## Working pipeline
* Merging reads using pandaseq
* Aligning merged reads to the novel allele reference using balst
* Filtering blast output to an identity of at least 96%
* Filtering reads to match novel alleles SNPs
* Creating a select set of forward and reverse fastq files